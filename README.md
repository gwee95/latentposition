# Latent Position

## Overview
This project attempts to model network data using latent positions by incorporating a multiplicative gamma process prior which will be able to intrinsically estimate the optimal number of dimensions for the latent space model. 
References using:

[(Hoff. D. Peter et al. 2002)](https://www.jstor.org/stable/pdf/3085833.pdf)

[(Bhattacharya A. et al. 2011)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3419391/)

[(Durante D. 2016)](https://arxiv.org/pdf/1610.03408.pdf)

[(Murphy K. et al. 2019)](https://arxiv.org/pdf/1701.07010.pdf)

[(Murphy K. Supplementary)](https://projecteuclid.org/download/suppdf_1/euclid.ba/1570586978)

